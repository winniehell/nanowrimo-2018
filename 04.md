# 4

- "Guten Tag!
   Fachhandel für Straßenbaubedarf, Sie sprechen mit Gerhard Geißbock."
- "Hallo! Hier spricht... äh... Martin Müller."

Einem Instinkt folgend nannte Herr Oink von Ploink nicht seinen richtigen Namen.
Sein Herz raste.
Er fühlte sich wie ein richtiger Krimineller.
Oder zumindest so, wie er dachte, dass sich ein richtiger Krimineller fühlen müsste, wenn er kriminelle Machenschaften durchführte.
Schnell fuhr er fort:

- "Ich würde gerne eine Absperrung für Gehwegschäden bestellen."
- "Welche Maße?"

Diese Frage traf Herrn Oink von Ploink unvorbereitet.
Wie dumm von ihm.
Natürlich brauchte er die Maße von dem Loch, wenn er eine Absperrung dafür bestellen wollte.
Wie konnte er das nur vergessen?
Schnell machte er eine Schätzung, um den professionellen Eindruck am Telefon zu wahren:

- "Äh... so... 4 Meter".
- "4 Meter lang? Und wie hoch?"
- "Einen Meter."
- "Einen Meter Höhe haben wir nicht.
   Wir liefern nur Standardmaße.
   1,50 ist das niedrigste was wir im Lager haben.
   Von welcher Abteilung waren sie nochmal?"
- "Abteilung für Straßenausbesserung, aber noch neu dabei.
   Dann nehme ich 4 mal 1,50."

Als Lieferadressse nannte Herr Oink von Ploink eine kleine, unbewohnte Straße in der Nähe seiner Wohnung.
Das Kriminalitätsgefühl intensivierte sich.

Während der Arbeit war er an diesem Tag sehr angespannt.
Immer wenn sich schnelle Schritte näherten, drehte er sich nervös um, nur um sich dann schnell wieder unauffällig seiner Beschäftigung zuzuwenden.
Das nahende Ende des Arbeitstags beruhigte ihn und es dauerte keine Minute bis er sich angezogen hatte und aus dem Büro verschwunden war.

Er hatte sich mit Oma und Herkules im Park verabredet.
Sich in einer der Wohnungen zu treffen schien ihnen zu riskant.
Auf einer Parkbank sitzend fand er die beiden.

- "Und, ist alles glatt gegangen, Oskar?"
- "Du solltest meinen Tarnnahmen benutzen, Oma.
   Ich bin jetzt Martin Müller."

Oma unterdrückte ein Kichern.

- "Entschuldigung, Martin!
   Für einen Moment habe ich dich mit meinem Enkel Oskar verwechselt.
   Das muss wohl das Alter sein.
   Da spielt einem das Gedächtnis einen Streich.
   Und, Martin, lief alles glatt?"
- "Ich musste ein wenig improvisieren, weil ich die Maße des Lochs nicht kannte.
   Aber ansonsten war die Bestellung erfolgreich."
- "Alles klar, dann sehen wir uns heute abend am vereinbarten Treffpunkt?"

Im Schutz der Dunkelheit schlich sich Herr Oink von Ploink unter seinem Decknamen Martin Müller in die unbewohnte Gasse.
Dabei sah er sich mehrmals um und vergewisserte sich, dass ihm niemand folgte.
Tatsächlich standen dort, angelehnt an die Hauswand, mehrere Absperrungen.
Und acht Betonklötze.
Martin Müller schlug sich mit der Hand vor die Stirn.
Natürlich – ohne Betonklötze würden die Absperrungen ja nicht stehen.
Als Mitarbeiter der Abteilung für Straßenausbesserung wüsste er das.
Wie sollten er, Oma und Herkules die Klötze transportieren?
Versuchsweise hob er einen an.
Er konnte ihn gerade so tragen.

Plötzlich hörte er dumpfe Schritte hinter der Hausecke.
Blitzschnell stellte er den Betonklotz ab, drehte sich um und stellte sich vor die Absperrungen.
Oma kam mit Herkules um die Ecke.
Erleichtert atmete er auf.

- "Hallo Martin! Schön Sie hier zu sehen!"

flüsterte Oma.

Martin Müller, dessen richtiger Name übrigens Herr Oink von Ploink war, trat einen Schritt zur Seite damit Oma die Absperrung sehen konnte.
Sie lächelte zufrieden.
Dann machte sie "Oh." als sie die Betonklötze sah.

Doch Martin Müller war entschlossen, so kurz vor dem Ziel nicht aufzugeben.
Sein Namensvetter hatte schließlich sogar eine verbesserte Rezeptur für Tomatensuppe erfunden.
Dann würde er sicherlich auch ein paar lächerliche Betonklötze tragen können.

Es dauerte eine Weile, alles zu Loch Nummer 1 zu tragen und dort aufzustellen.
Doch dank Oma und Herkules, die eine Vorhut bildeten, begegnete Martin Müller keinen anderen Menschen während des Transports.
Zufrieden blickten die drei nach getaner Arbeit auf ein abgesperrtes Loch.

- "Und das war nur der Anfang!"

kommentierte Martin Müller grimmig, der in Gedanken schon eine weitere Absperrung für Loch Nummer 2 bestellte.

- "Wir können viel erreichen.
   Gemeinsam retten wir im Geheimen die Bürger dieser Stadt."

ergänzte Oma.

Dann machten sie sich in verschiedene Richtungen auf den Heimweg.
Herr Oink von Ploink, der jetzt nicht mehr Martin Müller war, befand sich am Ende seiner Kräfte.
In seiner Wohnung angekommen legte er sich mit Mantel und Schuhen aufs Sofa und schlief mit einem Lächeln ein.

Er träumte davon, wie er als maskierter Superheld vor dem abgesperrten Loch posierte.
Die Menge jubelte ihm zu.
Eine wichtige Person mit Mikrofon hielt eine Lobesrede, die jäh von einem durchdringenden Piepen unterbrochen wurde.

Herr Oink von Ploink wachte neben dem Sofa auf und alles fühlte sich kaputt an.
Mühsam richtete er sich auf, schlurfte in die Küche und machte sich Frühstück.
Die Mühe, Mantel und Schuhe auszuziehen, ersparte er sich.
Der Gedanke an die vollbrachte Heldentat der letzten Nacht, heiterte ihn auf und überzeugte sogar seine Gesichtsmuskeln etwas Lächelartiges zu produzieren.

Auf dem Weg ins Büro war es voller als sonst.
Die Menschen stauten sich und waren nicht im gewohnten Eiltempo der morgendlichen Hektik unterwegs.
Einige Zeit später sah Herr Oink von Ploink den Grund dafür:
Mehrere Polizisten standen an dem abgesperrten Loch.
Zwei von ihnen leiteten die Menschen auf die Fahrbahn um, auf der ein weiterer Polizist neben einem Streifenwagen stand.
Ein anderer war damit beschäftigt, ein Absperrband um die Absperrung zu ziehen.

Herr Oink von Ploink sah sich die abgesperrte Absperrung und die ernst blickenden Polizisten an und ein mulmiges Gefühl stieg in ihm auf.
Kurz darauf hielt in einiger Entfernung ein Transporter mit quietschenden Reifen.
Darauf stand in großen Buchstaben: Sonderermittlungsgruppe Straßen- und Infrastrukturkriminalität (SEGSI).
Herrn Oink von Ploinks Herz machte einen Moment lang Pause als mehrere Personen in weißen Anzügen mit der Aufschrift "Spurensicherung" ausstiegen.

Schnell richtete er den Blick wieder nach vorne und versuchte, in der Menge schneller voran zu kommen, um den Ort möglichst schnell zu verlassen.
Es dauerte quälend lange, sich durch die zähe Masse aus menschlichen Körpern zu kämpfen.
Hier und da verzichtete Herr Oink und Ploink auch nicht auf den Einsatz von Ellenbogen.

Als er das Loch hinter sich gelassen hatte, rannte er fast zu seinem Arbeitsplatz.
Statt auf den Fahrstuhl zu warten, sprintete er die Treppe hoch.
Dabei ließ er einige Stufen aus und schwebte beinahe über die Treppenabsätze.
Oben angekommen musste er erstmal ein wenig verschnaufen.
Sein Herz raste – und das lag nicht nur an der sportlichen Betätigung.

Kurz bevor er seinen Schreibtisch erreichte, blieb er wie angewurzelt stehen.
Sein Vorgesetzter und ein Polizist waren in ein Gespräch vertieft.
Als sie ihn bemerkten, winkten sie ihn herbei.

- "Guten Morgen, Herr Oink von Ploink!
   SEGSI schickt mich.
   Ich habe mich mit Ihrem Vorgesetzten schon unterhalten, aber auch noch einige Fragen an Sie."

Herr Oink von Ploink nickte.
Sein Vorgesetzter führte sie zu einem gerade ungenutzten Besprechungsraum und ließ sie dann allein.

- "Es geht um eine Angelegenheit, die sich einige Straßen weiter ereignet hat.
   Vielleicht haben Sie schon davon gehört?"
- "Es liegt zufällig auf meinem Arbeitsweg.
   Eine schreckliche Sache!"
- "In der Tat!
   Jemand hat sich unbefugt in den Tätigkeitsbereich der Abteilung für Straßenausbesserung eingemischt und damit viele Menschenleben gefährdet.
   Leider muss ich Ihnen mitteilen, dass eine der Spuren, die wir momentan verfolgen, zu Ihnen führt."

Herr Oink von Ploink schluckte.

- "Zu mir?
   Aber ich habe doch auch erst heute morgen davon erfahren und da waren Ihre Kollegen schon vor Ort."
- "Niemand geht davon aus, dass Sie in die Straftat verwickelt sind, das kann ich Ihnen versichern.
   Ihr Vorgesetzter hat betont, dass er es für ausgeschlossen hält, dass einer seiner Mitarbeiter in kriminelle Machenschaften verwickelt ist.
   Dennoch wurde der Telefonanruf mit dem die Tatwerkzeuge bestellt wurden, zu Ihrem Schreibtisch zurück verfolgt."
- "Und was bedeutet das?"

fragte Herr Oink von Ploink, dem allmählich recht unwohl bei dieser Befragung war.

- "Wir vermuten zur Zeit, dass sich eine unbefugte Person Zutritt zu dem Gebäude verschafft hat und zufällig Ihren Arbeitsplatz für das Telefonat ausgewählt hat.
   Jedoch haben wir uns die hohen Sicherheitsstandards, die hier gelten demonstrieren lassen und es ist uns unklar wie jemand ohne Unterstützung eines Mitarbeiters diese überwinden konnte."
- "Sie meinen, der Täter hatte einen Komplizen?
   Oder die Täterin?"
- "Wir gehen momentan von mehreren Tätern aus.
   Unter anderem weil die Tatwerkzeuge laut einem Augenzeugen von Hand transportiert wurden.
   Sagt Ihnen der Name Martin Müller etwas?"
- "Ist das nicht der Mann aus der TV-Werbung?"

Der Polizist musste lachen.

- "Ja, den kennt wohl jeder.
   Meine Frau war von der Tomatensuppe mit verbesserter Rezeptur sehr begeistert.
   Sie konnte gar nicht glauben, dass jemand es geschafft hatte, die Rezeptur von Tomatensuppe zu verbessern.
   Ich kann mir aber wirklich nicht vorstellen, dass es sich bei dem TV-Star um unseren Täter handelt.
   Eine letzte Frage habe ich noch an Sie – reine Routine:
   Wo waren Sie gestern Abend und was haben Sie gemacht?"

Auf diese Frage war Herr Oink von Ploink vorbereitet:

- "Meine Oma hatte gestern einen ihrer vorzüglichen Marmorkuchen gebacken.
   Den haben wir gemeinsam gegessen und dann bin ich nach Hause gegangen.
   Ich war gestern sehr müde und bin früh schlafen gegangen."
- "Ach, ich liebe Marmorkuchen.
   Ihre Oma wird den gemeinsam verbrachten Abend ja sicherlich bestätigen können.
   Ich glaube, die Kollegen sind schon vor Ort.
   Merkwürdigerweise war auch sie in den Fall verwickelt: Die Tatwerkzeuge wurden von ihrem Bankkonto bezahlt.
   Können Sie sich das erklären?"
- "Absolut nicht!
   Da muss ein Bankirrtum vorliegen.
   Oma würde sich niemals an einer Straftat beteiligen."
- "Nunja, wir werden sehen.
   Sicherlich klärt sich das alles noch auf.
   Vielen Dank für Ihre Zeit, Herr Oink von Ploink!
   Falls es noch weitere Fragen gibt, weiß ich ja, wie ich Sie erreiche."

Herr Oink von Ploink verabschiedete sich und ging zurück an seinen Schreibtisch.
Dort klebte ein Notizzettel mit der Nachricht:

- "Bitte melden Sie sich umgehend bei Ihrem Vorgesetzten!"

Zum ersten Mal seit Herr Oink von Ploink in dieser Abteilung arbeitete wurde er von der persönlichen Assistentin direkt zu seinem Vorgesetzten hereingelassen.
Dieser war gerade am Telefonieren, gestikulierte aber, dass Herr Oink von Ploink sitch setzen solle.
Ein weiteres Mal verschlang der gemütliche Ledersessel also einen Großteil von Herrn Oink von Ploink. Der überlegte sich bei dieser Gelegenheit, ob sein Vorgesetzter vielleicht absichtlich einen Stuhl für sein Gegenüber ausgesucht hatte, der demjenigen das Gefühl gab, aufgefressen zu werden – auf sehr bequeme Art natürlich.
In dem Telefonat ging es wohl um irgendeinen Zeitungsartikel und dem Tonfall nach zu urteilen, konnte es sich bei dem Gesprächspartner nur um eine wichtige Person handeln.
Kurze Zeit später war das Gespräch beendet und Herr Oink von Ploinks Vorgesetzter wandte sich ihm zu.

- "Ich danke Ihnen, dass Sie so schnell kommen konnten, Herr Oink von Ploink.
   Es ist ja ein ganz schönes Schlamassel, in das Sie uns da reingeritten haben.
   Aber ich will ehrlich mit Ihnen sein: Es erlaubt uns auch in einigen wichtigen Tageszeitungen zu unseren hohen Sicherheitsstandards, die sogar von der Polizei lobend erwähnt wurden, Stellung zu nehmen.
   Gerade habe ich mit der Abteilungsleiterin telefoniert, die mit mir einer Meinung ist, dass wir diese Angelegenheit ganz groß aufziehen können.
   Ich sollte Ihnen also auch danken für diese einzigartige Möglichkeit.
   Doch nun zu dem eigentlichen Grund weshalb ich Sie hergebeten habe:
   Als ich von der Polizei befragt wurde, versicherte ich, dass keiner meiner Mitarbeiter jemals in kriminelle Machenschaften verwickelt wäre.
   Das habe ich genau so gemeint.
   Sollte ich also erfahren, dass gegen Sie berechtigt Ermittlungen eingeleitet worden sind, werden Sie nicht nur Ihren Arbeitsplatz in dieser Abteilung verlieren, sondern ich werde auch persönlich dafür sorgen, dass Sie in keiner anderen Abteilung eine Anstellung erhalten.
   Habe ich mich klar ausgedrückt?"
- "Allerdings, vielen Dank für Ihre Offenheit!"
- "Und jetzt gehen Sie nach Hause und ruhen sich aus.
   So eine polizeiliche Befragung erlebt man ja nicht jeden Tag.
   Sie sehen sehr erschöpft aus.
   Meine persönliche Assistentin wird Ihnen eine HATWAT ausstellen."
- "Ich danke Ihnen!
   Einen schönen Tag noch!"

Eine HATWAT, Hierarchisch Angeordnete Temporäre und Wiederrufbare Anweisung zur Tätigkeitsunterbrechung, wurde nur höchst selten ausgestellt.
Herr Oink von Ploink dachte sich insgeheim, dass sein Vorgesetzter vermutlich nur vermeiden wollte, dass ein potentiell Krimineller in seiner Abteilung herum lief.
Doch er war tatsächlich sehr erschöpft und hatte nichts gegen eine Pause einzuwenden.
So konnte er etwas früher als geplant Oma und Herkules einen Besuch abstatten.
