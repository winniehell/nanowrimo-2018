# Herr Oink von Ploink

Diese Geschichte wird von [@winniehell] geschrieben und ist unter der [Creative Commons Lizenz] (CC BY-SA 4.0) verfügbar.
Angefangen hat sie 2018 während dem [National Novel Writing Month].
Den vollständigen Text gibt es auch [als PDF](herr-oink-von-ploink.pdf).

[@winniehell]: https://winniehell.de
[National Novel Writing Month]: https://nanowrimo.org/participants/winniehell/novels/herr-oink-von-ploink/stats
[Creative Commons Lizenz]: http://creativecommons.org/licenses/by-sa/4.0/

---

repository verschoben nach https://framagit.org/winniehell-texte/oink-von-ploink
