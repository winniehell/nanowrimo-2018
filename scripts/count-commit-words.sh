#!/usr/bin/env bash

set -o errexit
set -o nounset

sha=$1

git show --word-diff=porcelain $sha | grep -e "^+[^+]" | wc -w
