#!/usr/bin/env bash

function show_count {
  find * \
    -maxdepth 0 \
    -regex "$1" \
    -exec cat {} \; \
    | tr -d '#-' \
    | wc -w
}

echo -n 'total: '
show_count '[0-9]*\.md'
echo -n 'today: '
show_count "$(date +%d)\.md"
