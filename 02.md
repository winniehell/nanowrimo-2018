# 2

Fast eine Woche war vergangen.
Jeden Morgen wachte Herr Oink von Ploink in der Hoffnung auf, dass irgendwer bestimmt irgendwas getan hatte.
Doch das Loch blieb.
Und er war sich ziemlich sicher, dass es tatsächlich größer wurde.
Die Menschen hatten es inzwischen in ihren Arbeitsweg integriert: Unmittelbare vor dem Loch teilten sie sich in zwei Gruppen auf und verschmolzen dahinter wieder zu einem Strom.
Nur vereinzelt stolperten einige von ihnen noch, weil sie in Gedanken vertieft oder noch nicht ganz aus Morpheus' Armen gehüpft waren.

Doch Herr Oink von Ploink war damit nicht zufrieden.
Ein Loch im Bürgersteig war falsch.
Es war zu einem Loch in seiner heilen Welt geworden.
Und eins stand fest: Jemand war verantwortlich dafür, dass es solche Löcher nicht gab.
Irgendjemand in der Abteilung für Straßenausbesserung, Unterabteilung Gehwegschäden arbeitete offensichtlich nicht mit maximaler Effizienz.
Irgendwo wurde eine durchschnittlichen Tagesleistung nicht erbracht.
Das war ungeheuerlich.
Das war... Faulheit!
Und Herr Oink von Ploink hatte sich fest vorgenommen, das zu korrigieren.

Am Tag zuvor hatte er seine Oma besucht und mit ihr selbstgebackenen Mamorkuchen gegessen.
Es gab viele Bäckereien in der Stadt und bei mehreren von ihnen konnte man Mamorkuchen kaufen.
Niemand zweifelte daran, dass diese Bäckereien effizient arbeiteten.
Doch Omas Mamorkuchen schmeckte einfach besser.
Herr Oink von Ploink konnte nicht genau sagen, was anders war.
Er hatte ihr schon oft beim Backen zugesehen und es schien nicht an einer geheimen Zutat zu liegen.
Vielleicht war es einfach eine Prise Omaliebe?

Wenn die beiden sich trafen, redeten meistens über alles mögliche.
Oma wollte wissen, wie die Welt da draußen so war – denn sie ging nur noch selten aus dem Haus.
Als Oma so alt war wie Herr Oink von Ploink jetzt, war die Welt noch eine andere.
Das war noch vor ARABU.
Alle Leute arbeiteten vollkommen chaotisch.
So wie Oma es erzählte, gab es Büros mit unterschiedlichen Arbeitszeiten, öffentliche Gebäude mit unterschiedlichen Öffnungszeiten und einige Menschen wie Ärztinnen und Krankenpfleger mussten sogar nachmittags oder nachts arbeiten.
Das verstand Herr Oink von Ploink nicht.
Wie sollte man sich in so einer ungeordneten Welt zurecht finden?

Auch der Verwaltungsapparat war damals... unvollständig.
Es gab Ämter, die vollkommen unterschiedliche Aufgaben vereinten.
Einige Sachbearbeiter hatten Aufgaben zugeteilt, die überhaupt nicht zusammengehörten.
Beispielsweise gab es Standesbeamte, die Dokumente vorbereiteten, Kundengespräche führten, Dokumente vorbereiteten und Eheschließungen durchführten.
Für Herrn Oink von Ploink war das unvorstellbar.
Wie sollte man so viele unterschiedliche Aufgaben an einem einzigen Arbeitstag schaffen?
Wie sollte man überhaupt jemanden finden, der für all dies qualifiziert war?

In der heutigen Welt gab es so etwas zum Glück nicht mehr.
Herr Oink von Ploink erinnerte sich an die Sachbearbeiterin aus der Abteilung für Straßenbau, die ihn an die Abteilung für Straßenausbesserung verwiesen hatte.
So war es richtig.
Es lag einfach nicht im Aufgabenbereich der Sachbearbeiterin sich um Straßenausbesserung zu kümmern.
Wenn jeder plötzlich die Aufgaben von irgendjemand anderem erledigte, würde alles im Chaos versinken.
Niemand wüsste, ob die Angelegenheit, mit der er oder sie sich jetzt beschäftigen würde, schon abgeschlossen wäre oder nicht.
Es müssten auf einmal Sachbearbeiter von ganz unterschiedlichen Abteilungen miteinander kommunizieren, damit sie korrekt arbeiteten.
Unvorstellbar!

Herr Oink von Ploink hatte Oma auch von seinem Lochproblem erzählt.
Auf ihrem Gesicht erschien ein Lächeln, aber sie guckte dabei ein wenig traurig.
Wie so oft, machte sie dann einen Witz bei dem Herr Oink von Ploink sich den Bauch halten musste vor Lachen.
Eigentlich wusste er gar nicht so genau, warum er sich den Bauch halten musste, wenn er laut lachte.
Aber Leute machten das eben so, wenn gute Witze erzählt wurden und Omas Witze waren die besten.

- "Warum sperrst du das Loch nicht selbst ab, Oskar?"

Als Herr Oink von Ploink, dessen Vorname übrigens Oskar war, fertig gelacht hatte, antwortete er grinsend:

- "Du meinst, ich mache eine Bestellung bei einem Fachhandel für Straßenbaubedarf, gehe damit zu dem Loch im Bürgersteig und stelle die Absperrung einfach auf?
   Du hast wirklich lustige Einfälle, Oma."
- "Das stimmt wohl."

meinte Oma und guckte noch ein bisschen trauriger.

Zurück an seinem Schreibtisch bei der Arbeit kam Herrn Oink von Ploink der Vorschlag von Oma gar nicht mehr so absurd vor.
Jemand mit genügend Geld und Einfluss war sicherlich in der Lage ihn umzusetzen.
Jemand wie Herr Müller zum Beispiel.
Er würde einfach beim Fachhandel für Straßenbaubedarf anrufen und sagen:

- "Hallo, hier spricht Herr Müller!
   Ich habe dort ein Loch im Bürgersteig entdeckt und benötige dringend eine Premiumabsperrung."

Die Antwort des Servicemitarbeiters der Servicehotline des Fachhandel für Straßenbaubedarf wäre dann sicherlich:

- "Oh, hallo Herr Müller!
   Ich bin ein großer Fan von der Tomatensuppe mit verbesserter Rezeptur, die sie neulich im Fernsehen vorgestellt haben.
   Es wäre mir eine große Ehre Ihnen die Premiumabsperrung mit verstärkter Eigenleuchtkraft noch heute zu liefern.
   Kostenlos selbstverständlich."

Wahrscheinlich wüsste Herr Müller auch Bescheid, wie man eine solche Absperrung korrekt aufstellt.
Herr Müller kannte sich schließlich mit solchen Dingen aus.

Doch Herr Oink von Ploink war nicht Herr Müller.
So blieb ihm nichts anderes übrig als sich an die zuständige Behörde zu wenden.
Er füllte also erneut ein VERBOT-Formular aus, um an diesem Tag früher aufzubrechen – gerade rechtzeitig, um hoffentlich noch einen Termin in der Abteilung für Straßenausbesserung zu bekommen.

Auf dem Weg zu seinem Vorgesetzten mit dem Formular in der Hand, wurde er diesmal von seinen Kollegen kaum beachtet.
Er hatte sich große Mühe gegeben, in den vergangenen Tagen höchstens durchschnittliche Arbeitsleistung zu erbringen.
Gelegentlich hatte er sogar die Mittagspause um ein paar Minuten überzogen und sich einmal absichtlich nicht in die Strichliste an der Kaffeemaschine eingetragen.

Auch diesmal war Herr Oink von Ploinks Vorgesetzter selbstverständlich in einem wichtigen Gespräch.
Eigentlich war es noch nie vorgekommen, dass er nicht in einem wichtigen Gespräch war, als Herr Oink von Ploink vorbei kam.
Er war eben eine wichtige Person und musste sich über wichtige Entscheidungen mit anderen wichtigen Personen austauschen, so dachte Herr Oink von Ploink.
Also nahm die persönliche Assistentin das Formular entgegen und versprach Herrn Oink von Ploink sofort zu informieren, wenn sein Vorgesetzter etwas Zeit für ihn hätte.

Nach zwei Stunden war es dann soweit und Herr Oink von Ploink wurde zu seinem Vorgesetzten gerufen.
Die Zwischenzeit hatte er damit verbracht, möglichst unauffällig schneller arbeiten.
Während sein Vorgesetzter noch besorgt das VERBOT-Formular durchlas, versank Herr Oink von Ploink in einem gemütlichen Ledersessel.
Er blickte sich um.
An den Wänden hingen Fotos von wichtigen Personen beim Golf Spielen oder beim Händeschütteln.
Jedenfalls vermutete er stark, dass es sich um wichtige Personen handelte.
Der Vorgesetzte räusperte sich.
Inzwischen hatte er seinen Blick vom Formular abgewandt und sah Herrn Oink von Ploink stirnrunzelnd an.

- "Wissen Sie, Herr Oink von Ploink, wir schätzen es sehr, wenn unsere Mitarbeiter einen Ausgleich zu ihrer harten Arbeit schaffen.
   Es ist wichtig, sich nicht von den beruflichen Aufgaben verschlingen zu lassen, sondern auch einen gewissen Raum für, sagen wir, so etwas wie Nebenbeschäftigungen zu schaffen.
   Dabei spreche ich, und das möchte ich noch einmal betonen, von Dingen, denen man sich widmet außerhalb der Arbeitszeit.
   Sie haben mich jetzt zum wiederholten Mal darum gebeten, sich innerhalb der Arbeitszeit für außerberuftliche Beschäftigungen freistellen zu lassen.
   Grundsätzlich bin ich da gerne bereit, im Ausnahmefall auf persönliche Bedürfnisse einzugehen.
   Es ist mir jedoch zu Ohren gekommen, dass Sie seit neustem eher unterdurchschnittliche Arbeitsleistung erbringen.
   Im Interesse Ihrer beruflichen Laufbahn, muss ich diesen Antrag also leider ablehnen.
   Ich hoffe, das verstehen Sie."
- "Natürlich, voll und ganz.
   Vielen Dank!"

sagte Herr Oink von Ploink, der nicht verstand, stand auf und bewegte sich in Richtung Tür.

- "Eins noch, bevor ich es vergesse.
   Von einem Ihrer Kollegen wurde mir berichtet, dass Sie die Strichliste an der Kaffeemaschine wohl nicht sehr gewissenhaft führen.
   Diese Maßnahme gilt der besseren Erfassung von Pausenzeiten und damit letzten Endes Ihrer eigenen Effizienz.
   Ich möchte, dass Sie sich dessen bewusst sind."
- "Selbstverständlich, ich bitte um Entschuldigung."

In Herrn Oink von Ploink entwickelte sich ein ungewohntes Gefühl.
Er verspürte plötzlich den Drang... unhöfliche Worte auszusprechen.
Nachdenklich verließ er das Büro.
Was hatte das wohl zu bedeuten?

Zurück an seinem Platz, überlegte Herr Oink von Ploink, was er wohl noch unternehmen konnte.
Bei seiner Arbeit war er sehr unkonzentriert und musste mehrmals Fehler in Berichten korrigieren.
Doch das störte ihn nicht, denn er hatte ja sowieso schon vorgearbeitet.
Es musste doch irgendetwas geben, das er, Herr Oink von Ploink, tun konnte.
Er wählte noch einmal die Nummer der Servicehotline von der Abteilung für Straßenausbesserung.
Die paradiesischen Klänge eines synthetischen Orchesters wichen einer automatischen Bandansage:

- "Vielen Dank für Ihren Anruf!
   Ihr Anliegen ist uns wichtig.
   Leider sind zur Zeit alle verfügbaren Servicemitarbeiter in Kundengesprächen.
   Bitte haben Sie einen Moment Geduld.
   Der nächste freie Mitarbeiter ist in Kürze für Sie verfügbar.
   Die voraussichtliche Wartezeit beträgt 9 Stunden."

Herr Oink von Ploink knallte etwas lauter als beabsichtigt den Hörer auf das Telefon.
Dann zuckte er zusammen als ihn plötzlich eine weibliche Stimme von hinten ansprach:

- "Hallo Herr Oink von Ploink!"

Er drehte sich um und Gina, seine Kollegin, die ein paar Schreibtische weiter arbeitete, kicherte.

- "Oh, äh, hallo! Was darf ich... ähm... was kann ich für Sie tun, Frau... äh?"

Herrn Oink von Ploink fiel ein, dass er den Nachnamen von Gina gar nicht kannte.
Komisch eigentlich – alle Kollegen sprach er mit Nachnamen an, aber in seinem Kopf besaß Gina nur einen Vornahmen.

- "Ach, nennen Sie mich doch einfach Gina"

sagte Gina.

- "Oh, achso, ja.
   Was kann ich denn für Sie tun... Gina?"
- "Ich habe mich gefragt, ob Sie heute Abend schon etwas vor haben."
- "Naja, also gewöhnlich sehe ich mir meine Lieblingsserie im Fernsehen an und..."
- "Prima! Dann gehen wir heute Abend gemeinsam Essen?"
- "Äh..."
- "Ich hole Sie ab.
   Ein paar Straßen weiter ist ein ganz entzückendes, kleines Restaurant, wo wir ungestört sind."
- "Mmmh..."

brummte Herr Oink von Ploink, was Gina wohl als Zustimmung wertete.

- "Bis später dann!"

flötete sie mit einem fast schon zu freundlichen Lächeln und tänzelte zurück an ihren Schreibtisch.

Herr Oink von Ploink war überrascht.
Nicht im negativen Sinne – gegen ein Abendessen mit Gina war nichts einzuwenden.
Doch er fühlte sich unvorbereitet.
Sollte er Blumen kaufen?
Vielleicht auch eine neue Krawatte?

Er entschied sich dafür einen kleinen Strauß zu bestellen und war mit den Gedanken an das Abendessen so sehr beschäftigt, dass er das Loch im Bürgersteig vollkommen verdrängt hatte – bis er auf dem Heimweg plötzlich davor stand.
Einen kurzen Moment hielt er inne, dann ging er weiter.
Nein, jetzt war nicht der passende Augenblick, um über eine neue Lösung für sein Problem nachzudenken.
Sollte er Gina davon erzählen?
Vielleicht hatte sie ja eine Idee, was man wohl tun könnte.

Herr Oink von Ploink hatte es geschafft zu Duschen, ein neues Hemd anzuziehen und sogar den Blumenstrauß auszupacken als es klingelte.
In einem mittleren Eiltempo rannte er langsam die Treppe herunter.
Er war sich selbst nicht so ganz sicher, welches wohl ein angemessenes Tempo war.
Unten angekommen begrüßte ihn Gina in einem glitzernden, roten Kleid und mit einem strahlenden Lächeln.
Sie bedankte sich für die Blumen und hakte sich dann bei Herrn Oink von Ploink unter.

- "Ich heiße übrigens Oskar."

sagte Herr Oink von Ploink, dessen Vorname übrigens Oskar war.

Gina erzählte davon, wie sie das schnuckelige kleine Restaurant zufällig entdeckt hatte, als sie mit ein paar Freunden unterwegs war.
Sie berichtete davon, zu welchen Anlässen sie welche Gerichte ausprobiert hatte und was sie davon empfehlen konnte.
Sie erzählte auch davon, wie freundlich doch das Personal dort war und dass sie einmal fast ihre Handtasche vergessen hätte und der Kellner ihr dann doch noch einen Häuserblock hinterher gelaufen war, um sie ihr zu bringen.
Ginas Redestrom endete erst als sie beim Restaurant angekommen waren.
Doch Herrn Oink von Ploink störte das nicht.
Auf diese Weise musste er nur gelegentliches Nicken und bestätigendes Brummen zur Unterhaltung beitragen.

Die Innenwände des Restaurants waren Felsen nachempfunden und es gab sogar einige plätschernde Wasserfälle.
Die Luft war angenehm frisch mit einem Hauch von würzigen Gerüchen.
Ein Kellner im Frack kam sofort auf sie zu und begleitete sie zu ihrem Tisch.
Mit dem Inhalt der Speisekarte war Herr Oink von Ploink ein wenig überfordert.
Schließlich wählte er einfach das aus, was Gina ihm empfahl.
Dazu nahm er ein stilles Wasser mit einem Spritzer Limette.
Er erinnerte sich noch gut daran, wie Herr Müller die gesunde Wirkung der Limetten erklärt hatte.

- "Jetzt, wo wir unter uns sind, Oskar:
   Wie geht es denn mit Ihrem VBZSDT voran?"
- "Oh, das beabsichtige ich gar nicht."
- "Wie? Aber alle sprechen doch davon."

Gina sah auf einmal sehr enttäuscht aus.
Herr Oink von Ploink verstand nicht, was gerade passierte.
Doch er bemühte sich, die Situation aufzuklären:

- "Ich habe in den vergangenen Tagen versucht, meine Aufgaben schneller zu abzuarbeiten, um etwas Zeit für außerberufliche Erledigungen einzusparen."

Und dann erzählte er von dem Loch im Bürgersteig und seinen Versuchen, die Angelegenheit selbst in die Hand zu nehmen.
Als er geendet hatte, war das Lächeln aus Ginas Gesicht verschwunden.
Ihr einziger Kommentar lautete:

- "Oh."

Herr Oink von Ploink versuchte das Thema zu wechseln, doch es gelang ihnen nicht, über längere Zeit eine Unterhaltung aufrecht zu erhalten.
Beide bestellten kein Dessert, sondern verließen das Restaurant und verabschiedeten sich draußen.
Herr Oink von Ploink wartete noch eine Weile, bis Gina nicht mehr zu sehen war.
Er drehte sich um und wollte sich betrübt auf den Heimweg machen, da bliebt er plötzlich erschrocken stehen.
Direkt vor ihm war ein Loch im Bürgersteig.
Dieses Loch war nicht das, was er kannte und es war noch nicht dort als sie das Restaurant betreten hatten.
Dieses Loch war neu.
